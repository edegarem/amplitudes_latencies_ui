function output = detect_threshold(dat, filename,system,gain,Ltube,step,reverse,alpha)

    cson = 340; % spee of sound
    risk = alpha; % risk alpha 
    
    % initialisation of ABR wave parameters
    hsgn = reverse; %1 is normal, -1 is in the animal facility visualisation
    
    %get sound level parameters
    Lmin=dat.stimparams.Lmin; 
    Lmax=dat.stimparams.Lmax; 
    dL=dat.stimparams.delL;
    L=Lmax:-abs(dL):Lmin;
    
    if(dL>0)
        L=L(end:-1:1); 
    end
    
    if strcmp(system,'CERIAH')
        L=1:size(dat.hrav,2);
    elseif strcmp(system,'Otophylab')
        L=dat.stimparams.L;    
    end
    
    fmin = dat.stimparams.fmin; 
    fmax = dat.stimparams.fmax; 
    if(isfield(dat.stimparams,'delf')) 
        df = dat.stimparams.delf;
    else 
        df = (fmax-fmin); 
    end
    if(df==0)
        f = fmin;
    elseif(df>0)
        f = fmin:df:fmax;
    elseif(df<0)
        f = fmax:df:fmin;
    end
    
    %1) Filter wave(s) whithin specified band
    fs   = dat.stimparams.fech; 
    t0   = 1e-3.*dat.stimparams.espace./2;
    ttube= Ltube/cson;
    tt   = 1e3*(dat.tt1-t0-ttube);
    hrfac= 1e6./gain;

    try 
        h=hsgn*hrfac.*dat.hrav;
    catch
        h=hsgn*hrfac.*dat.hravsing;
    end

    h    = h-mean(h);
    
    period_pre = tt(tt<0);
    period_signal = 1/fs:1/fs:12;
    period_signal = period_signal';

    switch system
        case{'CERIAH'}
            period_post = tt(tt>20);
        otherwise 
            period_post = tt(tt>12);
    end

    period_pre=length(period_pre);
    period_signal=length(period_signal);
    period_post=length(period_post);
    
    switch system
        case{'CERIAH'}
            previous_average = h(:,1);
            for i = 1:length(L)
                current_average = h(:,i);
                %here baseline at the end
                xc_pre=xcov(current_average(length(current_average)-period_post+1:end),previous_average(period_pre+(1:period_signal)),round(fs));
                threshold(i) = mean(xc_pre)+norminv(1-risk,0,1)*std(xc_pre);
                xc_signal(i) = max(xcov(current_average(period_pre+(1:period_signal)),previous_average(period_pre+(1:period_signal)),round(fs)));
            end
            figure('position',[200 50 800 500])
            subplot(1,2,1);
            hold on;
            plot(L,xc_signal,'-ok','markerfacecolor','r', 'linewidth',2);
            plot(L,threshold,'--m', 'linewidth',2);
            if all(threshold>0) && all(xc_signal>0)
                set(gca,'yscale','log')
            end
            box off;
            xlabel('Level (dB SPL)');
            ylabel('cross-correlation max');
            
            idx=find(xc_signal>threshold);
            plot(L(idx),xc_signal(idx),'ok','markerfacecolor','g', 'linewidth',2);
            thrsh = idx;
            legend({'Max Xcorr','Signif Threshold','Above threshold'},'location','northeast')

            subplot(1,2,2); hold on
            hrav = h; tt1 = dat.tt1; e = dat.stimparams.espace;
            naplot=1:size(hrav,2);
            nf=1:size(dat.hrav,3);
            plotlevel=0;
            hrge=step;
            for na=naplot
                % plot accumulated data in a separate figure
                mhrav=mean(hrav(:,na,nf));
                if(na==1)
                    plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel, 'g')
                    text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),['File' num2str(L(na))],'fontsize',11)
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')
                    xlabel('t (ms)')
                else
                    if any(na == idx)
                        plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'color', 'g')
                    else
                        plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'color', 'r')
                    end
                    text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),['File' num2str(L(na))],'fontsize',11)
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')    
                end               
                plotlevel=plotlevel+hrge;
            end

        otherwise % animal facility

             if size(h,3) > 1
                try
                    id = str2double(filename(end-16:end-15));
                    nf = f == id;
                    hrav = squeeze(h(:,:,nf));
                catch 
                    id = str2double(filename(end-15)); %5kHz
                    nf = f == id;
                    hrav = squeeze(h(:,:,nf));
                end
            else
                hrav = h;
             end
             
            tt1 = dat.tt1; 
            e = dat.stimparams.espace;
            plotlevel = 0;
            hrge = step;            

            for i = 1:length(L)-1
                previous_average = hrav(:,i);
                current_average = hrav(:,i+1);    
                xc_pre=xcov(current_average(1:period_pre),previous_average(period_pre+(1:period_signal)),round(fs));
                threshold(i) = mean(xc_pre)+norminv(1-risk,0,1)*std(xc_pre);
                xc_signal(i) = max(xcov(current_average(period_pre+(1:period_signal)), ...
                    previous_average(period_pre+(1:period_signal)),round(fs)));
            end
        
            figure('position',[200 50 800 500])
            subplot(1,2,1);hold on;
            xc_signal = [xc_signal(1) xc_signal];
            threshold = [threshold(1) threshold]; 
            plot(L,xc_signal,'-or','markerfacecolor','r');
            plot(L,threshold,'--k');    
            if all(threshold>0) && all(xc_signal>0)
                set(gca,'yscale','log')
            end
            box off;
            xlabel('Level (dB SPL)');
            ylabel('cross-correlation max');
            
            idx=find(xc_signal<threshold,1,'first');
            if isempty(idx) && length(find(xc_signal>threshold))==length(xc_signal)
                plot(L,xc_signal,'ok','markerfacecolor','g', 'linewidth',2);
            end
            if idx>1
                plot(L(1:idx-1),xc_signal(1:idx-1),'ok','markerfacecolor','g', 'linewidth',2);
            end
            idx2=find(xc_signal<threshold);
            try
                thrsh = L(idx2(1)-1);
            catch
                thrsh = NaN; 
            end
            legend({'Max Xcorr','Signif Threshold','Above threshold'},'location','northeast')

            subplot(1,2,2); hold on
            naplot = 1:size(hrav,2);
            nf = 1:size(hrav,3);

            for na=naplot
                % plot accumulated data in a separate figure
                mhrav=mean(hrav(:,na,nf));
                if(na==1)
                    plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel, 'g')
                    text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11)
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')
                    xlabel('t (ms)')
                else
                    if isempty(idx) && length(find(xc_signal>threshold))==length(xc_signal)
                        plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'color', 'g')
                    elseif any(na == (1:idx-1))
                        plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'color', 'g')
                    else
                        plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'color', 'r')
                    end
                    text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11)
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')    
                end               
                plotlevel=plotlevel-hrge;
            end

    end
    output = thrsh;
end