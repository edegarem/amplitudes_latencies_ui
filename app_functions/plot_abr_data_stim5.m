function hp=plot_abr_data_stim5(dat, computerId,fname,gain,step,reverse)
%plot of abr data in standard (juxtaposition of waveforms) or browser form 

hsgn = reverse; %1 is normal, -1 is in the animal facility

fmin = dat.stimparams.fmin; 
fmax = dat.stimparams.fmax; 
if(isfield(dat.stimparams,'delf')) 
    df = dat.stimparams.delf;
else 
    df = (fmax-fmin); 
end
if(df==0)
    f = fmin;
elseif(df>0)
    f = fmin:df:fmax;
elseif(df<0)
    f = fmax:df:fmin;
end
    
%noise levels
Lmin_nois = dat.stimparams.Lmin_nois; 
Lmax_nois = dat.stimparams.Lmax_nois;
dL_nois   = dat.stimparams.delL_nois; 
if(dL_nois==0) 
    Lnois=Lmin_nois;
elseif(dL_nois>0)
    Lnois=Lmin_nois:dL_nois:Lmax_nois; 
elseif(dL_nois<0)
    Lnois=Lmax_nois:dL_nois:Lmin_nois;
end

Lmin = dat.stimparams.Lmin; 
L = Lmin.*ones(1,length(Lnois)); 
e = dat.stimparams.espace;
%rescale abr waveform to �V units
hrfac = 1e6./gain; 
hrav = hsgn*hrfac.*dat.hrav;
tt1 = dat.tt1;

if strcmp(computerId,'ABR1')
    minhrge=hrfac./4;
elseif strcmp(computerId,'ABR2')
    minhrge=hrfac./20;
else 
    minhrge = 5; % Check for TDT  
end

disp(['waveform datasize ' num2str(size(hrav))])
hrge=step; % fixed step for easier comparison between differents acquisitions 
disp(['peak-peak amplitude: ' num2str(hrge)]);
disp(['minhrge: ' num2str(minhrge)]);

naplot=1:size(hrav,2);
nfplot=1:size(hrav,3);
nplots=max(size(hrav,2),size(hrav,3));
for nf=nfplot
    f0 = f(nf);
    plotlevel=0;
    for na=naplot
        % plot accumulated data in a separate figure
        mhrav=mean(hrav(:,na,nf));
        if(na==1)
            figure, set(gcf,'position',[125 50 800 72*nplots])
            plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel)
            nfig=gcf; hold on
            plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')
            text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB and noise ' num2str(Lnois(na)) 'dB'],'fontsize',11)  
            xlabel('t (ms)')
            title(['f = ' num2str(f(nf)) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
        else 
            figure(nfig),hold on
            plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel)
            plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')    
            text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB and noise ' num2str(Lnois(na)) 'dB'],'fontsize',11)
        end               
        plotlevel=plotlevel - hrge;
    end
    if any(strcmp(computerId,{'ABR1', 'ABR2'}))
        set(gca,'xlim',[-15 20])
    end
    saveas(figure(nfig),[fname(1:end-4) '-' 'rABR.jpeg'])
end

        
end

