function compare_peak_deb_fin(filename,level,g)

%filename = 'Z:\Projets\2022-05-PEA_analysis-CERIAH\Dataset\ABR1\ABR mamp\882\5khz.mat';


%% Load data
load(filename)
try
    load([filename(1:end-4) '_raw.mat'])
catch
    disp('No raw data detected or not in the good format')
    return
end

%% Cleaning data if required

selfactor = 10;
h_tmp = cell(size(hrtp,3),1);
h_filt = zeros(size(hrtp,1),size(hrtp,3));
for ind_dB = 1:size(hrtp,3)
    isel = [];
    for j = 1:size(hrtp,2)
        hr0 = hrtp(:,j,ind_dB);
        hmax= max(hr0,[],1); 
        hmin= min(hr0,[],1); 
        amptest=(hmax-hmin)./2; 

        Yperc = mean(abs(hr0));
        hh = hr0(abs(hr0)<Yperc);
        hstd = std(hh);

        if amptest<=selfactor*hstd
            isel = cat(1, isel, j);
        end
    end
    h_tmp{ind_dB} = hrtp(:,isel,ind_dB);
    h_filt(:,ind_dB) = sum(h_tmp{ind_dB},2);
end

%% Chose dB value (ici level=1 = la valeur la plus forte)
data = h_tmp{level};
nwaves = 5;
%colorpalette = ['c', 'r', 'g', 'b', 'm'];

%% Fatigue ?
gain = stimparams.gain; 
hrfac=1e6./gain; 
data = hrfac.*data;

h_debut = data(:,1:round(size(data,2)/2)); 
h_fin = data(:,round(size(data,2)/2)+1:end);

% figure; 
% plot(1000*tt1-stimparams.espace/2, mean(h_debut,2),'k')
% hold on
% plot(1000*tt1-stimparams.espace/2, mean(h_fin,2),'b')
% xlabel('Time (ms)', 'fontsize', 16)
% ylabel('Volts', 'fontsize', 16)
% legend('Part1', 'Part2')

tt = 1000*tt1-stimparams.espace/2;
interval = find(tt>1 & tt<10);

pks = 0.2*ones(1,nwaves); 
locs_pks = 200*(1:nwaves);
nodes = zeros(1,nwaves);
locs_nodes = 200*(1:nwaves);

%% DEBUT 
hf = mean(h_debut,2);
htp0=hf(tt<=0,:);
sigma0=std(htp0);

[pks_tmp,locs_pks_tmp] = findpeaks(hf(interval),'MinPeakProminence',sigma0);
[nodes_tmp,locs_nodes_tmp] = findpeaks(-hf(interval),'MinPeakProminence',sigma0);

pks(1:min(length(pks_tmp), nwaves)) = pks_tmp(1:min(length(pks_tmp), nwaves));
locs_pks(1:min(length(pks_tmp), nwaves)) = locs_pks_tmp(1:min(length(locs_pks_tmp), nwaves));
nodes(1:min(length(nodes_tmp), nwaves)) = nodes_tmp(1:min(length(nodes_tmp), nwaves));
locs_nodes(1:min(length(locs_nodes_tmp), nwaves)) = locs_nodes_tmp(1:min(length(locs_nodes_tmp), nwaves));

yy_peaks = pks;
yy_nodes = -nodes;            

%xx_peaks = tt(interval(locs_pks)); 
%xx_nodes = tt(interval(locs_nodes)); 

% for i = 1:min(length(pks), length(nodes))
%     nroi = drawpoint('Position',[xx_nodes(i) yy_nodes(i)], 'Color', colorpalette(i), 'Label', ['N' num2str(i)], 'Tag', ['N' num2str(i)]);
%     proi = drawpoint('Position',[xx_peaks(i) yy_peaks(i)], 'Color', colorpalette(i), 'Label', ['P' num2str(i)], 'Tag', ['P' num2str(i)]);
% end

amplitudeI   = distance_vert(yy_peaks(1), yy_nodes(1));
amplitudeII  = distance_vert(yy_peaks(2), yy_nodes(2));
amplitudeIII = distance_vert(yy_peaks(3), yy_nodes(3));
amplitudeIV  = distance_vert(yy_peaks(4), yy_nodes(4));
amplitudeV   = distance_vert(yy_peaks(5), yy_nodes(5));
amp_deb = [amplitudeI amplitudeII amplitudeIII amplitudeIV amplitudeV];

%% FIN 
hf = mean(h_fin,2);
htp0=hf(tt<=0,:);
sigma0=std(htp0);

[pks_tmp,locs_pks_tmp] = findpeaks(hf(interval),'MinPeakProminence',sigma0);
[nodes_tmp,locs_nodes_tmp] = findpeaks(-hf(interval),'MinPeakProminence',sigma0);

pks(1:min(length(pks_tmp), nwaves)) = pks_tmp(1:min(length(pks_tmp), nwaves));
locs_pks(1:min(length(pks_tmp), nwaves)) = locs_pks_tmp(1:min(length(locs_pks_tmp), nwaves));
nodes(1:min(length(nodes_tmp), nwaves)) = nodes_tmp(1:min(length(nodes_tmp), nwaves));
locs_nodes(1:min(length(locs_nodes_tmp), nwaves)) = locs_nodes_tmp(1:min(length(locs_nodes_tmp), nwaves));

yy_peaks = pks;
yy_nodes = -nodes;            

%xx_peaks = tt(interval(locs_pks)); 
%xx_nodes = tt(interval(locs_nodes)); 

% for i = 1:min(length(pks), length(nodes))
%     nroi = drawpoint('Position',[xx_nodes(i) yy_nodes(i)], 'Color', colorpalette(i), 'Label', ['N' num2str(i)], 'Tag', ['N' num2str(i)]);
%     proi = drawpoint('Position',[xx_peaks(i) yy_peaks(i)], 'Color', colorpalette(i), 'Label', ['P' num2str(i)], 'Tag', ['P' num2str(i)]);
% end

amplitudeI   = distance_vert(yy_peaks(1), yy_nodes(1));
amplitudeII  = distance_vert(yy_peaks(2), yy_nodes(2));
amplitudeIII = distance_vert(yy_peaks(3), yy_nodes(3));
amplitudeIV  = distance_vert(yy_peaks(4), yy_nodes(4));
amplitudeV   = distance_vert(yy_peaks(5), yy_nodes(5));
amp_fin = [amplitudeI amplitudeII amplitudeIII amplitudeIV amplitudeV];


%% PLOT
g;
plot(1:5,amp_deb,'k-*')
hold on
plot(1:5,amp_fin,'b-*')
legend('Part1', 'Part2')

end